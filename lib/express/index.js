var express = require ('express'),
       http = require ('http'),
     config = require ('../configuration'),
         //adding database connection to a MongoDB
         db = require ('../db'),
  heartbeat = require ('../routes/heartbeat'),
    //adding a project route
    project = require ('../routes/project'),
   notFound = require ('../middleware/notFound'),
        app = express ();

        //to parse the requested body when the forms are submitted
        app.use(express.bodyParser());

        app.set ('port', config.get("express:port"));
        
        app.get ('/heartbeat', heartbeat.index);
        //logger:
        app.use(express.logger({immediate:true, format:'dev'}));
        //handling 404 not Found errors:
        app.use(notFound.index);

        //saving (posting) a new Project
        app.post('/project', project.post);

        http.createServer(app).listen(app.get('port'));
        
        module.exports = app;


