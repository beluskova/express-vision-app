var app = require ('../app'),
request = require ('supertest');

//a test to check if the app's main page is working
describe ('vision heartbeat api', function(){
  describe ('when requesting resource /heartbeat', function(){
    it ('should respond with 200', function (done){
      request (app)
      .get ('/heartbeat')
      .expect('Content-Type', /json/)
      .expect(200, done);
    });
  });
});

//a test to check the 404 Not Found response
describe ('vision heartbeat api', function(){
	describe ('when requesting resource /missing', function(){
		it('should respond with 404', function(done){
			request(app)
			.get('/missing')
			.expect('Content-Type', /json/)
			.expect(404, done);
		})
	});

});